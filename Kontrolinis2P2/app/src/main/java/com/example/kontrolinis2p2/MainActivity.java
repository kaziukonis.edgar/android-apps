package com.example.kontrolinis2p2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    String broadcastName = "1_Programos_nesist_trans";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);

        IntentFilter filtras = new IntentFilter(broadcastName);
        registerReceiver(new Transl_Imtuvas_1(), filtras);
    }

    public class Transl_Imtuvas_1 extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent i) {
            Log.e("AAA","Received");
            Bundle extras = i.getExtras();

            String pranesimas = "Gauta naujas transliavimo pranešimas \n";
            pranesimas += "Intentas: " + i.getAction() + "\n";
            pranesimas += "Siuntėjas: " + extras.getString("AppName") + "\n";
            pranesimas += "Papildoma informacija: " + extras.getString("Message") + "\n";


            textView.setText(pranesimas);
            broadcastIntent();
        }
    }

    private void broadcastIntent() {
        Intent intent = new Intent();
        intent.setAction("2_Programos_nesist_isreikst_trans");
        intent.putExtra("Message", "Pavadinimas: " + broadcastName);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> infoItems = packageManager.queryBroadcastReceivers(intent, 0);
        for (ResolveInfo info : infoItems) {
            ComponentName cn = new ComponentName(info.activityInfo.packageName,
                    info.activityInfo.name);
            intent.setComponent(cn);
            sendBroadcast(intent);
        }

    }

}