package com.example.lab4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = (WebView)findViewById(R.id.webview); //get webView
        webView.setWebViewClient(new WebViewClient()); //set webView client
        WebSettings webSettings = webView.getSettings();// initiate webView settings
        webSettings.setJavaScriptEnabled(true); //allow webView perform javascript
        webSettings.setBuiltInZoomControls(true); //show zoom control
    }

    public void onSearch(View view) {
        EditText webUrlInput = (EditText)findViewById(R.id.weburl);
        String webUrl = webUrlInput.getText().toString();
        Log.d("webUrl",webUrl);
        webView.loadUrl(webUrl);
    }
}