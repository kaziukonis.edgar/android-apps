package com.example.galutinis;

public class Product {
    int Id;
    String name;
    float price;

    Product(int id, String name, float price) {
        this.Id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return name; // You can add anything else like maybe getDrinkType()
    }
}
