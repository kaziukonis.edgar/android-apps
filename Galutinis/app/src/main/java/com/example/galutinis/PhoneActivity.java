package com.example.galutinis;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class PhoneActivity extends AppCompatActivity {

    String phoneNumber = "";
    EditText phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);
        phone = findViewById(R.id.editTextPhone);
    }

    public void dialNumber(View view) {
        switch (view.getId()) {
            case (R.id.button0):
                phoneNumber = phoneNumber + "0";
                break;
            case (R.id.button1):
                phoneNumber = phoneNumber + "1";
                break;
            case (R.id.button2):
                phoneNumber = phoneNumber + "2";
                break;
            case (R.id.button3):
                phoneNumber = phoneNumber + "3";
                break;
            case (R.id.button4):
                phoneNumber = phoneNumber + "4";
                break;
            case (R.id.button5):
                phoneNumber = phoneNumber + "5";
                break;
            case (R.id.button6):
                phoneNumber = phoneNumber + "6";
                break;
            case (R.id.button7):
                phoneNumber = phoneNumber + "7";
                break;
            case (R.id.button8):
                phoneNumber = phoneNumber + "8";
                break;
            case (R.id.button9):
                phoneNumber = phoneNumber + "9";
                break;
        }
        phone.setText(phoneNumber);
    }

    public void grazintiRezultatus(View w){
        String ivestasTekstas = phone.getText().toString();
        Intent PirmasLangas = new Intent(PhoneActivity.this, MainActivity.class);
        PirmasLangas.putExtra("ivestis", ivestasTekstas);

        setResult(Activity.RESULT_OK, PirmasLangas); // OK! (use whatever code you want)

        finish();
    }
}