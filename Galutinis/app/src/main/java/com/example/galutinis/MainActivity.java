package com.example.galutinis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Product> products = new ArrayList<Product>();
    ArrayList<Product> selectedProducts = new ArrayList<Product>();
    Spinner spinner;
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.productsSpinner);

        products.add(new Product(1,"Product 1", 10));
        products.add(new Product(2,"Product 2", 20));
        products.add(new Product(3,"Product 3", 30));

        ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(this,
                android.R.layout.simple_spinner_item, products);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Product product = (Product) parent.getSelectedItem();
                displayUserData(product);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ListView listView = (ListView) findViewById(R.id.productsView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Product product = (Product) parent.getItemAtPosition(position);
                displayUserData(product);
            }
        });

    }

    public void addProduct(View v) {
        Product product = (Product) spinner.getSelectedItem();
        selectedProducts.add(product);
        ListView listView = (ListView) findViewById(R.id.productsView);
        ArrayAdapter<Product> listAdapter = new ArrayAdapter<Product>(this,
                android.R.layout.simple_spinner_item, selectedProducts);
        listAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listView.setAdapter(listAdapter);
    }

    public void getSelectedProduct(View v) {
        Product product = (Product) spinner.getSelectedItem();
        displayUserData(product);
    }
    private void displayUserData(Product user) {
        String name = user.name;
        float price = user.price;
        String productData = "Name: " + name + "\nPrice: " + price;
        Toast.makeText(this, productData, Toast.LENGTH_LONG).show();
    }

    public void openPhoneActivity(View v) {
        Intent a = new Intent(this,PhoneActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(a, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("ivestis");
                Log.i("Phone number:", result);
                phoneNumber = result;
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("check", "NOT OK");
            }
        }
    }

    public  void call(View v)
    {
        // Find the EditText by its unique ID

        // show() method display the toast with message
        // "clicked"
        Toast.makeText(this, "clicked", Toast.LENGTH_LONG)
                .show();

        // Use format with "tel:" and phoneNumber created is
        // stored in u.
        Uri u = Uri.parse("tel:" + "867888971");

        // Create the intent and set the data for the
        // intent as the phone number.
        Intent i = new Intent(Intent.ACTION_DIAL, u);

        try
        {
            // Launch the Phone app's dialer with a phone
            // number to dial a call.
            startActivity(i);
        }
        catch (SecurityException s)
        {
            // show() method display the toast with
            // exception message.
            Toast.makeText(this, "An error occurred", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public  void message(View v)
    {
        // Find the EditText by its unique ID

        // show() method display the toast with message
        // "clicked"
        Toast.makeText(this, "clicked", Toast.LENGTH_LONG)
                .show();

        // Create the intent and set the data for the
        // intent as the phone number.

        String textnum = phoneNumber;
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.setData(Uri.parse("sms:" + textnum));
        String listOfProducts = "";
        for(Product product: selectedProducts) {
            listOfProducts = product.name + "\n" + listOfProducts;
        }
        smsIntent.putExtra("sms_body"  , listOfProducts);

        try
        {
            // Launch the Phone app's dialer with a phone
            // number to dial a call.
            startActivity(smsIntent);
        }
        catch (SecurityException s)
        {
            // show() method display the toast with
            // exception message.
            Toast.makeText(this, "An error occurred", Toast.LENGTH_LONG)
                    .show();
        }
    }
}