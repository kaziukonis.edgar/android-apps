package com.example.lab5;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WithoutA extends Fragment {

        TextView textView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.with_a, container, false);
            Bundle bundle = this.getArguments();
            String string = bundle.getString("text");
            textView = view.findViewById(R.id.textView);

            int vowels = 0;
            int uppercase = 0;
            for (int i = 0; i < string.length(); i++) {
                char temp = string.charAt(i);
                if (Character.isUpperCase(temp)) uppercase++;
                temp = Character.toLowerCase(temp);
                if (temp == 'a' || temp == 'e' || temp == 'i' || temp == 'o' || temp == 'u')
                    vowels++;
            }
            textView.setText(
                    "Word has '" + string.length() + "' letters \n"
                    + vowels + " vowels \n"
                    + uppercase + " uppercase \n"
                    + (string.length() - uppercase) + " lowercase");

            return view;
        }
}
