package vgtu.iip.lab2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class PirmaActivity extends AppCompatActivity {

    final static int IVEDIMAS = 1;
    TextView isvedimoLaukas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pirma);
        isvedimoLaukas = (TextView) findViewById(R.id.tekstas);
    }

    public void atvertiVeiklaRezultatuGavimui(View w){
        Log.e("RC", "atvertiVeiklaRezultatuGavimui");
        Intent antrasLangas = new Intent(PirmaActivity.this, AntraActivity.class);
        startActivityForResult(antrasLangas, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("TODO", "Grizo is AntraActivity");
        if(requestCode == 1) {
            String result = data.getStringExtra("ivestis");
            //TODO patikrinti nuo kokios veiklos ir kokius rezultatus gavo, tada pasiimti siunciama reiksme ir ja paduoti vietoj sekancioje eiluteje irasyto teksto
            isvedimoLaukas.setText(result);
        }
    }

    public void sukurtiExplicitIntent(View w){
        String tekstas = isvedimoLaukas.getText().toString();
        Log.i("TODO", "reikia sukurti explicit intent su perduodamais duomenimis");
        //TODO sukurti explicit intent, su putExtra perduoti tekstas kintamaji ir tada paleisti, nelaukiant rezultatu
        Intent treciasLang = new Intent(PirmaActivity.this, TreciaActivity.class);
        treciasLang.putExtra("ivestis", tekstas);
        startActivity(treciasLang);
    }

    public void sukurtiImplicitIntent(View w){
        String duomenysSiuntimui = isvedimoLaukas.getText().toString();
        Log.i("TODO", "reikia sukurti implicit intent");
        //TODO sukurti explicit intent, jame perduoti kintamaji duomenysSiuntimui (tekstiniai duomenys)
        Intent treciasLang = new Intent(PirmaActivity.this, TreciaActivity.class);
        treciasLang.putExtra("ivestis", duomenysSiuntimui);
        //TODO tada kitas intent bus tam, kad vartotojas galetu rinktis norima programa ir ja paleisti nelaukaint rezultatu
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, duomenysSiuntimui);
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }
}
