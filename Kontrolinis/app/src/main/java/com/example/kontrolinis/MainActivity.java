package com.example.kontrolinis;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicBoolean;
import java.time.temporal.ChronoUnit;

public class MainActivity extends AppCompatActivity {

    TextView isved_lauk;
    Boolean timerWithDate = false;
    RandomNum numGenerator = new RandomNum();
    String mainDate;
    LocalDate startDate;
    LocalDate endDate;
    int mainYear;
    int mainMonth;
    int mainDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isved_lauk = findViewById(R.id.textView);
    }

    public void showDialog(View v) {
        Calendar cal
                = new GregorianCalendar();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onDateSet(DatePicker datePicker, int newYear, int newMonth, int newDay) {
                        mainYear = newYear;
                        mainMonth = newMonth;
                        mainDay = newDay;
                        Calendar cal
                                = new GregorianCalendar();
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        int month = cal.get(Calendar.MONTH);
                        int year = cal.get(Calendar.YEAR);
                        String text = year + "-" + month + "-" + day + " / " + newYear + "-" + newMonth + "-" + newDay;
                        isved_lauk.setText(text);
                        startDate = LocalDate.of(year, month, day);
                        endDate = LocalDate.of(newYear, newMonth, newDay);
                        long noOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
                        isved_lauk.setText(startDate.toString() + " / " + endDate + " Days:" + noOfDaysBetween);
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        String prevText = isved_lauk.getText().toString();
                                        mainDate = prevText.split(" Days:")[0];
                                        isved_lauk.setText(prevText.split(" Days:")[0]);
                                    }
                                },
                                5000
                        );
                        startTimerThread();
                    }
                }, year, month, day);
        datePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startTimerThread(){
        long days = ChronoUnit.DAYS.between(startDate, endDate);
        StopRunningThread();
        numGenerator = new RandomNum((int) days, 0, isved_lauk);
        new Thread(numGenerator).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startTimerThreadWithDate(View v){
        StopRunningThread();
        timerWithDate = true;
        long days = ChronoUnit.DAYS.between(startDate, endDate);
        numGenerator = new RandomNum((int) days, 0, isved_lauk);
        new Thread(numGenerator).start();
    }

    public void kill(View v) {
        isved_lauk.setText(null);
        StopRunningThread();
        finish();
    }

    public void handleEndOfGeneration(View v){
        StopRunningThread();
    }

    public void StopRunningThread(){
        if(numGenerator.isRunning()){
            numGenerator.stop();
        }
    }


    class RandomNum implements Runnable {

        AtomicBoolean running = new AtomicBoolean(false);
        int max;
        int min;
        int range;
        int rand;
        TextView isved_lauk;

        RandomNum(){}

        RandomNum(int max, int min, TextView isved_lauk) {
            this.max = max;
            this.min = min;
            this.range = max - min + 1;
            this.isved_lauk = isved_lauk;
        }

        public boolean isRunning(){
            return running.get();
        }

        public void stop(){
            running.set(false);
        }

        public void run() {
            running.set(true);
            if(!timerWithDate)
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            while (running.get()) {
                rand = (int) (Math.random() * range) + min;
                runOnUiThread(new Runnable() {
                    @Override
                    public void  run(){
                        if(timerWithDate) {
                            isved_lauk.setText(mainDate + '\n' + String.valueOf(rand));
                        } else {
                            isved_lauk.setText(String.valueOf(rand));
                        }

                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}